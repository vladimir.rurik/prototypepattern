namespace PrototypePattern
{
    public class Motorcycle : Vehicle, IMyCloneable<Motorcycle>
    {
        public string engineSize { get; set; }

        public Motorcycle() { }

        public Motorcycle(Motorcycle other) : base(other)
        {
            this.engineSize = other.engineSize;
        }

        public new Motorcycle Clone()
        {
            return new Motorcycle(this);
        }
    }
}