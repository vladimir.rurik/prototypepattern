namespace PrototypePattern
{
    public class Truck : Vehicle, IMyCloneable<Truck>
    {
        public string bedLength { get; set; }
        public int towingCapacity { get; set; }

        public Truck() { }

        public Truck(Truck other) : base(other)
        {
            this.bedLength = other.bedLength;
            this.towingCapacity = other.towingCapacity;
        }

        public new Truck Clone()
        {
            return new Truck(this);
        }
    }
}