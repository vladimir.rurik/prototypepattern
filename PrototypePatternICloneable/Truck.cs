namespace PrototypePattern
{
    public class Truck : Vehicle, IMyCloneable<Truck>, ICloneable
    {
        public string bedLength { get; set; }
        public int towingCapacity { get; set; }

        public Truck() { }

        public Truck(Truck other) : base(other)
        {
            this.bedLength = other.bedLength;
            this.towingCapacity = other.towingCapacity;
        }

        object ICloneable.Clone()
        {
            return this.Clone(); // Reuse the existing IMyCloneable.Clone() 
        }

        public new Truck Clone()
        {
            return new Truck(this);
        }
    }
}