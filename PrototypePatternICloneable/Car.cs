namespace PrototypePattern
{
    public class Car : Vehicle, IMyCloneable<Car>, ICloneable
    {
        public int numDoors { get; set; }
        public bool hasSunroof { get; set; }

        public Car() { }

        public Car(Car other) : base(other) // Call base constructor
        {
            this.numDoors = other.numDoors;
            this.hasSunroof = other.hasSunroof;
        }

        object ICloneable.Clone()
        {
            return this.Clone(); // Reuse the existing IMyCloneable.Clone() 
        }

        public new Car Clone() // 'new' to hide Vehicle.Clone()
        {
            return new Car(this);
        }
    }
}