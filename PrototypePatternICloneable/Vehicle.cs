﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern
{
    public class Vehicle : IMyCloneable<Vehicle>, ICloneable
    {
        public string type { get; set; }
        public string color { get; set; }
        public int numWheels { get; set; }

        public Vehicle() { }

        public Vehicle(Vehicle other) // Copy constructor for cloning
        {
            if (other == null)
            {
                throw new ArgumentNullException(nameof(other), "Provided Vehicle instance for cloning cannot be null.");
            }

            this.type = other.type;
            this.color = other.color;
            this.numWheels = other.numWheels;
        }

        object ICloneable.Clone()
        {
            return this.Clone(); // Reuse the existing IMyCloneable.Clone() 
        }

        public Vehicle Clone()
        {
            return new Vehicle(this);
        }
    }
}
