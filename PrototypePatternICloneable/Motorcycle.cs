namespace PrototypePattern
{
    public class Motorcycle : Vehicle, IMyCloneable<Motorcycle>, ICloneable
    {
        public string engineSize { get; set; }

        public Motorcycle() { }

        public Motorcycle(Motorcycle other) : base(other)
        {
            this.engineSize = other.engineSize;
        }

        object ICloneable.Clone()
        {
            return this.Clone(); // Reuse the existing IMyCloneable.Clone() 
        }

        public new Motorcycle Clone()
        {
            return new Motorcycle(this);
        }
    }
}