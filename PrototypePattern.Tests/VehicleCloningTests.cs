using NUnit.Framework;
using PrototypePattern;

namespace PrototypePattern.Tests
{

    [TestFixture]
    public class VehicleCloningTests
    {
        [Test]
        public void CarClone_CreatesDeepCopy()
        {
            var originalCar = new Car
            {
                type = "Sedan",
                color = "Silver",
                numWheels = 4,
                numDoors = 4,
                hasSunroof = true
            };

            var clonedCar = originalCar.Clone();

            // Assert that objects are not the same reference 
            Assert.AreNotSame(originalCar, clonedCar);

            // Assert properties are deeply copied
            Assert.AreEqual(originalCar.type, clonedCar.type);
            Assert.AreEqual(originalCar.color, clonedCar.color);
            Assert.AreEqual(originalCar.numWheels, clonedCar.numWheels);
            // ... (assert remaining properties)
            Assert.AreEqual(originalCar.numDoors, clonedCar.numDoors);
            Assert.AreEqual(originalCar.hasSunroof, clonedCar.hasSunroof);

            // Modify cloned object to verify it's a deep copy
            clonedCar.color = "Blue";

            Assert.AreNotEqual(originalCar.color, clonedCar.color);
        }

        [Test]
        public void TruckClone_CreatesDeepCopy()
        {
            var originalTruck = new Truck
            {
                type = "Pickup",
                color = "Red",
                numWheels = 4,
                bedLength = "6ft",
                towingCapacity = 10000
            };

            var clonedTruck = originalTruck.Clone();

            // Assert that objects are not the same reference 
            Assert.AreNotSame(originalTruck, clonedTruck);

            // Assert properties are deeply copied
            Assert.AreEqual(originalTruck.type, clonedTruck.type);
            Assert.AreEqual(originalTruck.color, clonedTruck.color);
            Assert.AreEqual(originalTruck.numWheels, clonedTruck.numWheels);
            // ... (assert remaining properties)
            Assert.AreEqual(originalTruck.bedLength, clonedTruck.bedLength);
            Assert.AreEqual(originalTruck.towingCapacity, clonedTruck.towingCapacity);

            // Modify cloned object to verify it's a deep copy
            clonedTruck.color = "Blue";

            Assert.AreNotEqual(originalTruck.color, clonedTruck.color);
        }

        [Test]
        public void MotorcycleClone_CreatesDeepCopy()
        {
            var originalMotorcycle = new Motorcycle
            {
                type = "Sport",
                color = "Black",
                numWheels = 2,
                engineSize = "1000cc",
            };

            var clonedMotorcycle = originalMotorcycle.Clone();

            // Assert that objects are not the same reference 
            Assert.AreNotSame(originalMotorcycle, clonedMotorcycle);

            // Assert properties are deeply copied
            Assert.AreEqual(originalMotorcycle.type, clonedMotorcycle.type);
            Assert.AreEqual(originalMotorcycle.color, clonedMotorcycle.color);
            Assert.AreEqual(originalMotorcycle.numWheels, clonedMotorcycle.numWheels);
            // ... (assert remaining properties)
            Assert.AreEqual(originalMotorcycle.engineSize, clonedMotorcycle.engineSize);

            // Modify cloned object to verify it's a deep copy
            clonedMotorcycle.color = "Blue";

            Assert.AreNotEqual(originalMotorcycle.color, clonedMotorcycle.color);
        }
    }
}